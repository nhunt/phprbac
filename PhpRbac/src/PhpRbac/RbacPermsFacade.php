<?php

namespace PhpRbac;

use Illuminate\Support\Facades\Facade;

class RbacPermsFacade extends Facade
{

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
    protected static function getFacadeAccessor() { return 'rbac.perms'; }

}
