<?php

namespace PhpRbac;

use Illuminate\Support\Facades\Facade;

class RbacRolesFacade extends Facade
{

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
    protected static function getFacadeAccessor() { return 'rbac.roles'; }

}
