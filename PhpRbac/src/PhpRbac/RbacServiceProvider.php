<?php

namespace PhpRbac;

use Illuminate\Support\ServiceProvider;

class RbacServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->app->bindShared('rbac', function ($app)
        {
            return new Rbac;
        });

        $this->app->bindShared('rbac.roles', function ($app)
        {
            return with(new Rbac)->Roles;
        });

        $this->app->bindShared('rbac.perms', function ($app) 
        {
            return with(new Rbac)->Permissions;
        });

        $this->app->bindShared('rbac.user', function ($app) 
        {
            return with(new Rbac)->Users;
        });
    }

    public function provides()
    {
        return ['rbac', 'rbac.roles', 'rbac.perms', 'rbac.user'];
    }
}
