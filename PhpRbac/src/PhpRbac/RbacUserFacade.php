<?php

namespace PhpRbac;

use Illuminate\Support\Facades\Facade;

class RbacUserFacade extends Facade
{

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
    protected static function getFacadeAccessor() { return 'rbac.user'; }

}
